/*
Implementation of vr_viewer_pipe.h.
Compile using cmake.
*/

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <iostream>
#include <string>
#include <sstream>
#include <Eigen/Geometry>
#include "vr_viewer_pipe.h"

// constructor
vr_viewer_pipe::vr_viewer_pipe(const char* name){
    int res;
    pipe_name = name;
    res = mkfifo(pipe_name, 0666);
    if (res != 0){
        std::cout << "Could not create fifo " << pipe_name << "\n";
        pipe_fd = -1;
        exit(EXIT_FAILURE);
    }
    pipe_fd = open(pipe_name, O_WRONLY);
    std::cout << "Opened fifo " << pipe_name << "\n";
}

// destructor
vr_viewer_pipe::~vr_viewer_pipe(){
    close(pipe_fd);
    std::cout << "Closed fifo " << pipe_name << "\n";
    remove(pipe_name);
    std::cout << "Removed fifo " << pipe_name << "\n";
}

// write tracking data into named pipe
int vr_viewer_pipe::write_tracking(Eigen::Vector3f pos, Eigen::Quaternionf rot){
    int res;
    std::string s;
    s = vr_viewer_pipe::to_string(pos, rot);
    const char *buf = s.c_str();
    res = write(pipe_fd, buf, strlen(buf));
    return res;
}

// stringify position vector and rotation quaternion
std::string vr_viewer_pipe::to_string(Eigen::Vector3f pos, Eigen::Quaternionf rot){
    std::stringstream ss;
    std::string s;
    ss << "(" << pos[0] << ", " << pos[1] << ", " << pos[2] << ")";
    ss << ";"; // separation
    // x, y, z, w
    ss << "(" << rot.coeffs()[2] << ", " << rot.coeffs()[1] << ", " << rot.coeffs()[0] << ", " << rot.coeffs()[3] << ")";
    ss << "\n";
    return ss.str();
}
