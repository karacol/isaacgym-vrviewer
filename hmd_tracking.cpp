/*
Implementation of hmd_tracking.h.
Compile using cmake.
*/

#include "hmd_tracking.h"

// constructor
hmd_tracking::hmd_tracking(const char* name){
    pipe = new vr_viewer_pipe(name);

    // initialise OpenVR system
    vr::HmdError error;
    vr_system = vr::VR_Init(&error, vr::VRApplication_Background);  // works too
    //vr_system = vr::VR_Init(&error, vr::VRApplication_Overlay);

    if (error != vr::VRInitError_None){
        vr_system = NULL;
        std::cout << "Unable to initialise VR runtime: " << vr::VR_GetVRInitErrorAsEnglishDescription(error) << "\n";
        exit(EXIT_FAILURE);
    }

    // find head-mounted display
    for (unsigned int i=0; i< vr::k_unMaxTrackedDeviceCount; i++){
        if (!vr_system->IsTrackedDeviceConnected(i))
            continue;
        if (vr_system->GetTrackedDeviceClass(i) != vr::TrackedDeviceClass_HMD)
            continue;
        hmd_id = i;
        std::cout << "Assigned hmd_id=" << hmd_id << "\n";
        break;
    }
}

//destructor
hmd_tracking::~hmd_tracking(){
    if (vr_system != NULL){
        vr::VR_Shutdown();
        vr_system = NULL;
    }
}

// extract rotation from HMD pose matrix
Eigen::Quaternionf hmd_tracking::get_rot(vr::HmdMatrix34_t mat){
    Eigen::Matrix3f eigen_mat;
    Eigen::Quaternionf q;
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            eigen_mat(i, j) = mat.m[i][j];
        }
    }
    q = eigen_mat;
    return q;
}

// extract position from HMD pose matrix
Eigen::Vector3f hmd_tracking::get_pos(vr::HmdMatrix34_t mat){
    Eigen::Vector3f pos;
    for (int i=0; i<3; i++){
        pos(i) = mat.m[i][3];
    }
    return pos;
}

// write HMD position and rotation to named pipe
bool hmd_tracking::run(){
	vr::TrackedDevicePose_t tracked_deviced_pose;
	Eigen::Vector3f pos;
	Eigen::Quaternionf rot;
    int res;

    if (!vr_system->IsTrackedDeviceConnected(hmd_id))
        return true;
    vr_system->GetDeviceToAbsoluteTrackingPose(vr::TrackingUniverseStanding, 0, &tracked_deviced_pose, 1);
    pos = hmd_tracking::get_pos(tracked_deviced_pose.mDeviceToAbsoluteTracking);
    rot = hmd_tracking::get_rot(tracked_deviced_pose.mDeviceToAbsoluteTracking);
    res = pipe->write_tracking(pos, rot);
    if (res == -1){
        // SIGPIPE
        return false;
    }
    return true;
}
