"""
Example IsaaacGym simulation using VRViewer.
Run with "--headless" to use VRViewerSensor instead.
"""

import sys
import math
from isaacgym import gymapi
from example_env import ExampleEnv
from vrviewer import VRViewer, VRViewerSensor

headless = False
for arg in sys.argv[1:]:
    if arg == "--headless":
        headless = True
        break

example = ExampleEnv(headless=headless)

# create VRViewer
if headless:
    hmd_pos = gymapi.Vec3(0, 0, 0)
    hmd_rot = gymapi.Quat.from_axis_angle(gymapi.Vec3(1, 0, 0), -math.pi/2)
    vr_viewer = VRViewerSensor(example.gym, example.env, example.sim, hmd_pos, hmd_rot)
else:
    cam_pos = gymapi.Vec3(0, 0, 0)
    cam_target = gymapi.Vec3(1, 0, 0)
    vr_viewer = VRViewer(example.gym, example.env, example.sim, example.viewer, cam_pos, cam_target)

# run simulation
running = True
alive = True
while running and alive:
    try:
        alive = vr_viewer.step()
        running = example.step()
    except KeyboardInterrupt:
        vr_viewer.destroy()
        sys.exit()