"""
Example IsaaacGym environment.
"""

import math
import numpy as np
from isaacgym import gymapi

class ExampleEnv(object):
    def __init__(self, y_up=True, r=50, headless=False):
        self.gym = gymapi.acquire_gym()
        self.y_up = y_up
        self.r = r
        self.headless = headless
        self.sim = self.__create_simulation()
        self.__create_ground()
        if not self.headless:
            self.viewer = self.__create_viewer()
        self.env = self.__create_environment()
        self.landmarks_low = []
        self.landmarks_high = []
        self.landmarks_close = []
        self.__create_landmarks()

    def __create_simulation(self):
        sim_params = gymapi.SimParams()
        if self.y_up:
            sim_params.up_axis = gymapi.UP_AXIS_Y
            sim_params.gravity = gymapi.Vec3(0.0, -9.8, 0.0)
        else:
            sim_params.up_axis = gymapi.UP_AXIS_Z
            sim_params.gravity = gymapi.Vec3(0.0, 0.0, -9.8)
        return self.gym.create_sim(0, 0, gymapi.SIM_PHYSX, sim_params)

    def __create_ground(self):
        plane_params = gymapi.PlaneParams()
        if self.y_up:
            plane_params.normal = gymapi.Vec3(0, 1, 0)
        else:
            plane_params.normal = gymapi.Vec3(0, 0, 1)
        plane_params.distance = 0
        plane_params.static_friction = 1
        plane_params.dynamic_friction = 1
        plane_params.restitution = 0
        self.gym.add_ground(self.sim, plane_params)

    def __create_viewer(self):
        cam_props = gymapi.CameraProperties()
        return self.gym.create_viewer(self.sim, cam_props)

    def __create_environment(self):
        spacing = 2.0
        lower = gymapi.Vec3(-spacing, 0.0, -spacing)
        upper = gymapi.Vec3(spacing, spacing, spacing)
        envs_per_row = 1
        return self.gym.create_env(self.sim, lower, upper, envs_per_row)

    def __to_vec_3(self, l):
        return [gymapi.Vec3(*tuple) for tuple in l]

    def __create_landmarks(self):
        angles = [0, 0.25*math.pi, 0.5*math.pi, 0.75*math.pi, math.pi, 1.25*math.pi, 1.5*math.pi, 1.75*math.pi]
        y_high = self.r * np.sin(0.25*math.pi)
        xs = self.r * np.sin(angles)
        zs = self.r * np.cos(angles)
        xs_close = 2 * np.sin(angles)
        zs_close = 2 * np.cos(angles)
        positions_low = self.__to_vec_3([(x, 1, z) for (x, z) in zip(xs, zs)])
        positions_high = self.__to_vec_3([(x, y_high, z) for (x, z) in zip(xs, zs)])
        positions_close = self.__to_vec_3([(x, 0, z) for (x, z) in zip(xs_close, zs_close)])
        rgbs = self.__to_vec_3([(0.5, 0.5, 0.5), (1, 0, 0), (1, 1, 0), (0, 1, 0), (0, 1, 1), (0, 0, 1), (1, 0, 1), (1, 1, 1)])
        for i in range(8):
            pose_low = gymapi.Transform()
            pose_low.p = positions_low[i]
            self.landmarks_low.append(self.add_sphere(pose_low, rgbs[i]))
            pose_high = gymapi.Transform()
            pose_high.p = positions_high[i]
            self.landmarks_high.append(self.add_box(pose_high, rgbs[i]))
            pose_close = gymapi.Transform()
            pose_close.p = positions_close[i]
            self.landmarks_close.append(self.add_small_sphere(pose_close, rgbs[i]))

    def add_box(self, pose, colour):
        asset_options = gymapi.AssetOptions()
        asset_options.disable_gravity = True
        box_asset = self.gym.create_box(self.sim, 1, 1, 1, asset_options)
        actor_handle = self.gym.create_actor(self.env, box_asset, pose)
        self.gym.set_rigid_body_color(self.env, actor_handle, 0, gymapi.MESH_VISUAL, colour)
        return actor_handle

    def add_sphere(self, pose, colour):
        asset_options = gymapi.AssetOptions()
        asset_options.disable_gravity = True
        sphere_asset = self.gym.create_sphere(self.sim, 1, asset_options)
        actor_handle = self.gym.create_actor(self.env, sphere_asset, pose)
        self.gym.set_rigid_body_color(self.env, actor_handle, 0, gymapi.MESH_VISUAL, colour)
        return actor_handle

    def add_small_sphere(self, pose, colour):
        asset_options = gymapi.AssetOptions()
        asset_options.disable_gravity = True
        sphere_asset = self.gym.create_sphere(self.sim, 0.1, asset_options)
        actor_handle = self.gym.create_actor(self.env, sphere_asset, pose)
        self.gym.set_rigid_body_color(self.env, actor_handle, 0, gymapi.MESH_VISUAL, colour)
        return actor_handle        

    def look_at(self, cam_pos, cam_target):
        self.gym.viewer_camera_look_at(self.viewer, self.env, cam_pos, cam_target)

    def step(self):
        if not self.headless:
            if self.gym.query_viewer_has_closed(self.viewer):
                self.gym.destroy_viewer(self.viewer)
                self.gym.destroy_sim(self.sim)
                return False
        # step physics
        self.gym.simulate(self.sim)
        self.gym.fetch_results(self.sim, True)
        # step graphics
        self.gym.step_graphics(self.sim)
        # synchronise visual update frequency with real time
        self.gym.sync_frame_time(self.sim)        
        if not self.headless:
            self.gym.draw_viewer(self.viewer, self.sim, True)
        return True