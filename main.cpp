/*
OpenVR application for tracking HMD.
Compile using cmake.
*/

#include <chrono>
#include <thread>
#include <csignal>
#include "hmd_tracking.h"
#include "sim_view.h"

int main(){
    const char* pipename = "/tmp/isaacgymviewerpipe";
    sim_view view;
    hmd_tracking tracking(pipename);    

    // ignore sigpipe when python process closes
    signal(SIGPIPE, SIG_IGN);

    bool running = true;
    int count = 0;
    while(running){
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        running = tracking.run();
        if (count == 3){
            view.run();
            count = 0;
        }
        else{
            count ++;
        }
    }

    return 0;
}
