/*
Header file for sim_view class. Starts OpenVR overlay. Continuously reads image data from memory and shows it.
Compile using cmake.
*/

#ifndef SIM_VIEW_H
#define SIM_VIEW_H

#include <openvr.h>
#include <iostream>

class sim_view
{
private:
    vr::IVRSystem* vr_system = NULL;
    vr::VROverlayHandle_t overlay_handle;
    vr::VROverlayError overlay_error;
    vr::TrackedDeviceIndex_t hmd_id;
    uint8_t* buf = NULL;
    void check_overlay_error();
    void find_hmd();
    void read_raw();
public:
    sim_view(/* args */);
    ~sim_view();
    void run();
};

#endif
