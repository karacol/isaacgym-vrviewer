import math
from multiprocessing import Process, Queue
from isaacgym import gymapi

import numpy as np

class VRViewer(object):
    """VRViewer class to read tracking data and have the viewer camera follow it."""
    def __init__(self, gym, env, sim, viewer, camera_pos, camera_target, pipe_path="/tmp/isaacgymviewerpipe"):
        """Constructor method. Creates the headset actor and starts the tracking process.

        Args:
            gym (isaacgym.gymapi.Gym): Gym singleton
            env (isaacgym.gymapi.Env): environment
            sim (isaacgym.gymapi.Sim): simulation handle
            viewer (isaacgym.gymapi.Viewer): viewer handle
            camera_pos (isaacgym.gymapi.Vec3): initial viewer camera position
            camera_target (isaacgym.gymapi.Vec3): initial viewer camera target
            pipe_path (str, optional): path to named pipe for communication with OpenVR process. Defaults to "/tmp/isaacgymviewerpipe".
        """
        self.gym = gym
        self.env = env
        self.sim = sim
        self.viewer = viewer
        self.camera_pos = camera_pos
        self.camera_target = camera_target
        self.pipe_path = pipe_path
        # multiprocessing to read tracking data from cpp
        self.q = Queue()
        self.poll_tracking_data = Process(target=self.__get_tracking_data)
        self.poll_tracking_data.start()

    def __follow(self, pos, rot):
        headset_transform = gymapi.Transform(pos, rot)
        cam_pos = headset_transform.transform_point(self.camera_pos)
        cam_target = headset_transform.transform_point(self.camera_target)
        self.gym.viewer_camera_look_at(self.viewer, self.env, cam_pos, cam_target)

    def __get_tracking_data(self):
        """
        Read tracking data from read pipe. This method is called from poll_tracking_data process.
        When tracking data is received, write into queue.
        """
        with open(self.pipe_path) as pipe:
            print(f"Opened pipe {self.pipe_path}.")
            try:
                for line in pipe:
                    pos, rot = line.split(";")
                    pos = gymapi.Vec3(*eval(pos))
                    # evaluated as x, y, z, w (which is correct)
                    rot = gymapi.Quat(*eval(rot))
                    self.q.put([pos, rot])
            except KeyboardInterrupt:
                return
        print(f"Closed pipe {self.pipe_path}.")

    def destroy(self):
        self.poll_tracking_data.terminate()

    def step(self):
        """Attempts to read HMD pose from multiprocessing queue and update HMD camera."""
        if self.poll_tracking_data.is_alive():   
            try:
                pos, rot = self.q.get_nowait()
                self.__follow(pos, rot)
                #self.gym.write_viewer_image_to_file(self.viewer, "/tmp/sim_view.png")
            except:
                pass
            return True
        else:
            return False

class VRViewerSensor(object):
    """VRViewerSensor class to read tracking data and have a sensor camera follow it.
    """
    def __init__(self, gym, env, sim,  hmd_pos, hmd_rot, pipe_path="/tmp/isaacgymviewerpipe"):
        """Constructor method. Creates the headset actor and starts the tracking process.

        Args:
            gym (isaacgym.gymapi.Gym): Gym singleton
            env (isaacgym.gymapi.Env): environment
            sim (isaacgym.gymapi.Sim): simulation handle
            hmd_pos (isaacgym.gymapi.Vec3): initial HMD position
            hmd_rot (isaacgym.gymapi.Quat): initial HMD rotation
            pipe_path (str, optional): path to named pipe for communication with OpenVR process. Defaults to "/tmp/isaacgymviewerpipe".
        """
        self.gym = gym
        self.env = env
        self.sim = sim
        self.hmd_pos = hmd_pos
        self.hmd_rot = hmd_rot
        self.pipe_path = pipe_path
        # camera data
        self.camera_offset = gymapi.Vec3(1, 0, 0)
        self.camera_rotation = gymapi.Quat.from_axis_angle(gymapi.Vec3(1, 0, 0), 0)
        # create HMD actor + camera
        self.__create_hmd_actor()
        self.__attach_camera_to_hmd()
        # multiprocessing to read tracking data from cpp
        self.q = Queue()
        self.poll_tracking_data = Process(target=self.__get_tracking_data)
        self.poll_tracking_data.start()

    def __get_tracking_data(self):
        """
        Read tracking data from read pipe. This method is called from poll_tracking_data process.
        When tracking data is received, write into queue.
        """
        with open(self.pipe_path) as pipe:
            print(f"Opened pipe {self.pipe_path}.")
            try:
                for line in pipe:
                    pos, rot = line.split(";")
                    pos = gymapi.Vec3(*eval(pos))
                    # evaluated as x, y, z, w (which is correct)
                    rot = gymapi.Quat(*eval(rot))
                    self.q.put([pos, rot])
            except KeyboardInterrupt:
                return
        print(f"Closed pipe {self.pipe_path}.")

    def __create_hmd_actor(self):
        """Creates Gym actor representing HMD for debugging purposes.

        Args:
            pos (isaacgym.gymapi.Vec3): 3D position of HMD
            axis (isaacgym.gymapi.Vec3): unit vector indicating axis of HMD rotation
            angle (float): angle describing magnitude of HMD rotation about axis
        """
        # create asset
        asset_options = gymapi.AssetOptions()
        asset_options.disable_gravity = True
        asset = self.gym.create_sphere(self.sim, 1, asset_options)
        # actor transform
        self.hmd_transform = gymapi.Transform()
        self.hmd_transform.p = self.hmd_pos
        self.hmd_transform.r = self.hmd_rot
        # actor and body handle
        self.hmd_actor = self.gym.create_actor(self.env, asset, self.hmd_transform)
        self.hmd_body = self.gym.get_actor_rigid_body_handle(self.env, self.hmd_actor, 0)

    def __attach_camera_to_hmd(self):
        """Attaches a sensor camera to the HMD body."""
        self.camera_handle = self.gym.create_camera_sensor(self.env, gymapi.CameraProperties())
        camera_transform = gymapi.Transform(self.camera_offset, self.camera_rotation)
        self.gym.attach_camera_to_body(self.camera_handle, self.env, self.hmd_body, camera_transform,
                                       gymapi.FOLLOW_TRANSFORM)

    def __move_hmd(self, pos, rot):
        """Updates HMD actor pose.

        Args:
            pos (isaacgym.gymapi.Vec3): HMD position
            rot (isaacgym.gymapi.Quat): quaternion describing HMD rotation
        """
        body_states = self.gym.get_actor_rigid_body_states(self.env, self.hmd_actor, gymapi.STATE_ALL)
        body_states["pose"]["p"].fill((pos.x, pos.y, pos.z))
        body_states["pose"]["r"].fill((rot.w, rot.x, rot.y, rot.z))
        self.gym.set_actor_rigid_body_states(self.env, self.hmd_actor, body_states, gymapi.STATE_ALL)

    def destroy(self):
        self.poll_tracking_data.terminate()

    def step(self):
        """Attempts to read HMD pose from multiprocessing queue and update HMD camera."""
        if self.poll_tracking_data.is_alive():
            try:
                pos, rot = self.q.get_nowait()
                self.__move_hmd(pos, rot)
                self.gym.render_all_camera_sensors(self.sim)
                # array containing image data, 900x6400
                #self.gym.write_camera_image_to_file(self.sim, self.env, self.camera_handle, gymapi.IMAGE_COLOR, "/tmp/sim_view.png")
                image = self.gym.get_camera_image(self.sim, self.env, self.camera_handle, gymapi.IMAGE_COLOR)
                np.savetxt("viewing_tests/imagedata", image, fmt='%3.0f', delimiter=' ', newline=' ')
            except:
                pass
            return True
        else:
            return False
