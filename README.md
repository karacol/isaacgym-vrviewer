# Isaac Gym VRViewer

**Note:** This repository is no longer maintained, as I have reimplemented this project using ROS ([see here](https://gitlab.com/karacol/isaac_gym_for_vr)). 

Control the viewer of your Isaac Gym simulation using a VR headset by running a SteamVR application and adding a few lines of code into your simulation definition.

## Structure

The loop in [`main.cpp`](main.cpp) wraps around the HMD pose tracking from [`hmd_tracking.cpp`](hmd_tracking.cpp) which writes the tracking data into a named pipe.
The python class [`VRViewer`](vrviewer.py) wraps around the Isaac Gym `Viewer`.
It reads the tracking data from the named pipe and adjusts the viewer camera pose accordingly.

## Requirements

> Note: This project has only been tested on Linux.

To be able to build using OpenVR, install [Steam](https://store.steampowered.com/about/) and [SteamVR](https://www.steamvr.com/de/).

### Python

- [Isaac Gym](https://developer.nvidia.com/isaac-gym)

### C++

- [Eigen](eigen.tuxfamily.org/)

## Build

The C++ files of this project need to be built using `cmake`.
Adjust the includes in [`CMakeLists.txt`](CMakeLists.txt#L15) to point to the path of your `Eigen` installation.

```bash
mkdir build
cd build
cmake ..
cmake --build . --target isaacgym_vrviewer
```

The executables are now found in `build`.

## Usage

In your Isaac Gym simulation, create the `VRViewer`:

```py
from vrviewer import VRViewer

cam_pos = gymapi.Vec3(0, 0, 0)
cam_target = gymapi.Vec3(1, 0, 0)
vr_viewer = VRViewer(gym, env, sim, viewer, cam_pos, cam_target)
```

In the simulation loop, call `vr_viewer.step()`.

You can find an example implementation in [`example.py`](example.py).

### Run

Make sure your VR Headset is connected and SteamVR is recognising it properly.
Then, in one terminal, run

```bash
./build/isaacgym_viewer
```

Run your Isaac Gym simulation from another terminal.

### Debugging

To test things without SteamVR running and a VR headset at the ready, build the files in [`dummy_no_hmd/`](dummy_no_hmd/):

```bash
cd build
cmake ..
cmake --build . --target dummy_data
```

Test the named pipe outside the Isaac Gym context, without OpenVR:

```bash
./build/dummy_data
# another terminal
python3 dummy_read.py
```

Test the named pipe in the Isaac Gym context, without OpenVR:

```bash
./build/dummy_data
# another terminal
python3 example.py
```
