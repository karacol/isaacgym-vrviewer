/*
Testing program for writing to named pipe.
Compile using cmake.
*/

#include "dummy_tracker.h"
#include <chrono>
#include <thread>
#include <csignal>

int main(){
    const char* name = "/tmp/isaacgymviewerpipe";
    dummy_tracker tracker(name);
    bool b = true;

    // ignore sigpipe
    signal(SIGPIPE, SIG_IGN);

    while (b){
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
        b = tracker.run();
    }
    return 0;
}
