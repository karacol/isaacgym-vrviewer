/*
Implementation of dummy_tracker.h.
Compile using cmake.
*/

#include "dummy_tracker.h"

// constructor
dummy_tracker::dummy_tracker(const char* name){
    pipe = new vr_viewer_pipe(name);
    make_data();
}

//destructor
dummy_tracker::~dummy_tracker(){
    delete(pipe);
}

void dummy_tracker::make_data(){
    Eigen::Vector3f p(0.107417, 0.256933, 0.00598295);
    Eigen::Quaternionf r(-0.100236, 0.199346, -0.402985, 0.887591);
    data_pos.push_back(p);
    data_rot.push_back(r);
    p = Eigen::Vector3f(0.0356548, 0.392052, 0.0757723);
    r = Eigen::Quaternionf(-0.321534, 0.137033, -0.372508, 0.859695);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.03725, 0.722208, 0.21809);
    r = Eigen::Quaternionf(-0.0496166, -0.0508069, -0.0288339, 0.997058);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0682476, 0.690749, 0.21573);
    r = Eigen::Quaternionf(0.121952, -0.0756009, 0.0275252, 0.98927);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0486051, 0.698913, 0.208224);
    r = Eigen::Quaternionf(0.162525, -0.0755941, 0.0219658, 0.983559);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0619932, 0.675059, 0.19673);
    r = Eigen::Quaternionf(0.114165, -0.0729499, 0.0211537, 0.990554);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0586123, 0.638582, 0.211686);
    r = Eigen::Quaternionf(-0.0515114, -0.0858993, 0.0224309, 0.994718);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0507616, 0.6262, 0.228671);
    r = Eigen::Quaternionf(-0.0803536, -0.101027, 0.0240858, 0.991341);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.014294, 0.61452, 0.203864);
    r = Eigen::Quaternionf(-0.129996, -0.211397, 0.019543, 0.96852);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0637125, 0.64591, 0.235983);
    r = Eigen::Quaternionf(-0.0177007, -0.0624884, 0.042036, 0.997003);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.1885, 0.643197, 0.328579);
    r = Eigen::Quaternionf(0.028941, 0.517097, 0.0526041, 0.853818);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.220122, 0.639202, 0.375692);
    r = Eigen::Quaternionf(0.0351606, 0.592378, 0.0587615, 0.802745);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.153439, 0.642876, 0.274037);
    r = Eigen::Quaternionf(0.000663039, 0.235693, 0.0493805, 0.970572);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0457707, 0.643652, 0.247664);
    r = Eigen::Quaternionf(-0.0258872, -0.0640188, 0.0396533, 0.996825);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.0414752, 0.644025, 0.288483);
    r = Eigen::Quaternionf(-0.0459875, -0.401689, 0.0288318, 0.914166);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.0628322, 0.645297, 0.374812);
    r = Eigen::Quaternionf(-0.0445909, -0.642832, 0.0216087, 0.764403);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.0658618, 0.646286, 0.398014);
    r = Eigen::Quaternionf(-0.0477116, -0.700135, 0.0181196, 0.712184);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.047921, 0.647045, 0.392113);
    r = Eigen::Quaternionf(-0.0381508, -0.677803, 0.0218864, 0.733927);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.0353274, 0.647143, 0.333793);
    r = Eigen::Quaternionf(-0.0346534, -0.508754, 0.0223959, 0.859923);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0319928, 0.646661, 0.251366);
    r = Eigen::Quaternionf(-0.0298989, -0.136422, 0.0312549, 0.989706);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0628177, 0.675565, 0.264679);
    r = Eigen::Quaternionf(0.108827, -0.0796057, 0.0481502, 0.989698);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0836804, 0.715733, 0.278272);
    r = Eigen::Quaternionf(0.343771, -0.0627934, 0.0614655, 0.934933);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0817297, 0.714871, 0.277071);
    r = Eigen::Quaternionf(0.346751, -0.0720715, 0.0607413, 0.93321);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0768535, 0.706717, 0.269497);
    r = Eigen::Quaternionf(0.295676, -0.0918154, 0.0637534, 0.948726);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0690104, 0.655361, 0.238117);
    r = Eigen::Quaternionf(0.0282247, -0.0684728, 0.055625, 0.995701);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0642011, 0.589147, 0.225332);
    r = Eigen::Quaternionf(-0.242959, -0.0516478, 0.0465162, 0.967543);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0646553, 0.542864, 0.20008);
    r = Eigen::Quaternionf(-0.393823, -0.0506131, 0.0338104, 0.917169);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0677826, 0.529104, 0.196497);
    r = Eigen::Quaternionf(-0.439963, -0.0493897, 0.0391528, 0.895801);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0682595, 0.531438, 0.196991);
    r = Eigen::Quaternionf(-0.433554, -0.0492639, 0.038417, 0.89896);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0649176, 0.588728, 0.211259);
    r = Eigen::Quaternionf(-0.260859, -0.0732604, 0.0396377, 0.961777);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0838693, 0.644803, 0.246825);
    r = Eigen::Quaternionf(-0.0482405, -0.0727149, 0.0417089, 0.995312);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.180349, 0.638087, 0.246612);
    r = Eigen::Quaternionf(-0.0514095, -0.0631961, 0.0978089, 0.991865);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.210848, 0.635295, 0.235204);
    r = Eigen::Quaternionf(-0.0586872, -0.0556463, 0.0876148, 0.992866);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.126775, 0.641546, 0.239339);
    r = Eigen::Quaternionf(-0.0601971, -0.0751133, 0.0371437, 0.994663);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.00763446, 0.639837, 0.260326);
    r = Eigen::Quaternionf(-0.0545875, -0.0701197, -0.0151475, 0.995929);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.106779, 0.624457, 0.284694);
    r = Eigen::Quaternionf(-0.040828, -0.077774, -0.0935743, 0.99173);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.0966147, 0.628413, 0.285569);
    r = Eigen::Quaternionf(-0.0411614, -0.077322, -0.0752216, 0.993312);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.045947, 0.643403, 0.25601);
    r = Eigen::Quaternionf(-0.0520106, -0.0566716, 0.0119753, 0.996965);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0470216, 0.635832, 0.169868);
    r = Eigen::Quaternionf(-0.103582, -0.0623073, 0.0176195, 0.992511);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0335345, 0.606562, 0.0346758);
    r = Eigen::Quaternionf(-0.183758, -0.0588375, 0.0155381, 0.981086);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0364646, 0.616397, 0.0636843);
    r = Eigen::Quaternionf(-0.165258, -0.0606203, 0.0179156, 0.984223);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.063094, 0.63715, 0.193165);
    r = Eigen::Quaternionf(-0.0973197, -0.0553093, 0.0257897, 0.99338);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0788482, 0.643381, 0.281266);
    r = Eigen::Quaternionf(-0.0456028, -0.056126, 0.033423, 0.996822);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.104386, 0.638059, 0.386452);
    r = Eigen::Quaternionf(0.022922, -0.0540686, 0.0465438, 0.997189);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.117, 0.637729, 0.387871);
    r = Eigen::Quaternionf(0.0253418, -0.0508428, 0.0498578, 0.997139);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0870366, 0.642902, 0.263311);
    r = Eigen::Quaternionf(-0.0550554, -0.0627365, 0.0351321, 0.995891);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0692002, 0.640226, 0.227762);
    r = Eigen::Quaternionf(-0.0821862, -0.0522824, 0.0331603, 0.994692);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0502386, 0.626733, 0.225193);
    r = Eigen::Quaternionf(-0.134662, -0.0811016, 0.0407532, 0.986726);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0199248, 0.67355, 0.189505);
    r = Eigen::Quaternionf(-0.0535322, -0.160234, -0.0607384, 0.983753);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.025888, 0.586484, 0.180993);
    r = Eigen::Quaternionf(-0.551924, -0.0823704, -0.0487218, 0.828385);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(-0.0420725, 0.532539, 0.166748);
    r = Eigen::Quaternionf(-0.666963, -0.121418, 0.0468032, 0.73364);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.0817312, 0.601629, 0.0801328);
    r = Eigen::Quaternionf(-0.387963, -0.224032, -0.148649, 0.881588);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.338619, 0.343893, 0.115602);
    r = Eigen::Quaternionf(-0.28017, -0.397643, 0.0439557, 0.872612);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.603841, 0.204073, -0.0403079);
    r = Eigen::Quaternionf(-0.0469008, -0.186199, -0.0543217, 0.979887);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.632671, 0.137483, -0.0932514);
    r = Eigen::Quaternionf(-0.024366, -0.0949103, -0.0241798, 0.994894);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.632112, 0.131978, -0.0875132);
    r = Eigen::Quaternionf(0.0370512, -0.0870022, 0.0026813, 0.995515);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.632298, 0.132283, -0.087822);
    r = Eigen::Quaternionf(0.0372479, -0.0868047, 0.00282087, 0.995525);
    data_pos.push_back(p);
    data_rot.push_back(r);    
    p = Eigen::Vector3f(0.632353, 0.13243, -0.0879296);
    r = Eigen::Quaternionf(0.0372619, -0.0866551, 0.00300959, 0.995537);
    data_pos.push_back(p);
    data_rot.push_back(r);
}

bool dummy_tracker::run(){
    int res;
    if (counter < data_pos.size()) {
        res = pipe->write_tracking(data_pos[counter], data_rot[counter]);
        if (res == -1){
            return false;
        }
        counter ++;
        return true;
    }
    else {
        return false;
    }
}
