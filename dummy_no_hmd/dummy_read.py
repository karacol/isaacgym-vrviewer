"""
Testing file to read lines from named pipe.
"""

path = '/tmp/isaacgymviewerpipe'

with open(path, 'r') as pipe:
    print("Pipe opened")
    for line in pipe:
        print(line)
        
print("Pipe was closed")
