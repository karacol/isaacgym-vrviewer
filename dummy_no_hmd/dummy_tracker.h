/*
Class writing dummy tracking data into a named pipe.
Compile using cmake.
*/

#ifndef DUMMY_TRACKER_H
#define DUMMY_TRACKER_H

#include <Eigen/Geometry>
#include <unistd.h>
#include <vector>
#include <fstream>
#include <iostream>
#include "vr_viewer_pipe.h"

class dummy_tracker
{
private:
    vr_viewer_pipe* pipe = NULL;
    int counter = 0;
    std::vector<Eigen::Vector3f> data_pos;
    std::vector<Eigen::Quaternionf> data_rot;
    void make_data();
public:
    dummy_tracker(const char* name);
    ~dummy_tracker();
    bool run();
};


#endif