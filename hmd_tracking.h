/*
Header file for hmd_tracking class. Starts OpenVR overlay. Tracks HMD position and rotation and writes this data into a named pipe.
Compile using cmake.
*/

#ifndef HMD_TRACKING_H
#define HMD_TRACKING_H

#include <openvr.h>
#include <iostream>
#include <Eigen/Geometry>
#include "vr_viewer_pipe.h"

class hmd_tracking
{
private:
    vr::IVRSystem* vr_system = NULL;
    vr_viewer_pipe* pipe = NULL;
    int hmd_id = -1;
    Eigen::Quaternionf get_rot(vr::HmdMatrix34_t mat);
    Eigen::Vector3f get_pos(vr::HmdMatrix34_t mat);
public:
    hmd_tracking(const char* name);
    ~hmd_tracking();
    bool run();
};

#endif