/*
Header file for vr_viewer_pipe class.
Compile using cmake.
*/

#ifndef VR_VIEWER_PIPE_H
#define VR_VIEWER_PIPE_H

class vr_viewer_pipe
{
private:
    int pipe_fd;
    const char* pipe_name;
    std::string to_string(Eigen::Vector3f pos, Eigen::Quaternionf rot);
public:
    vr_viewer_pipe(const char* name);
    ~vr_viewer_pipe();
    int write_tracking(Eigen::Vector3f pos, Eigen::Quaternionf rot);
};

#endif
