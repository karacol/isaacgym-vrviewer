/*
Implementation of sim_view.h.
Compile using cmake.
*/

#include "sim_view.h"

// constructor
sim_view::sim_view(){
    vr::HmdError error;
    
    vr_system = vr::VR_Init(&error, vr::VRApplication_Overlay);

    if (error != vr::VRInitError_None){
        vr_system = NULL;
        std::cout << "Unable to initialise VR runtime: " << vr::VR_GetVRInitErrorAsEnglishDescription(error) << "\n";
        exit(EXIT_FAILURE);
    }

    // key, friendly name, handle
    overlay_error = vr::VROverlay()->CreateOverlay("sim_view", "sim_view", &overlay_handle);
    sim_view::check_overlay_error();
    std::cout << "Created Overlay\n";
	overlay_error = vr::VROverlay()->SetOverlayWidthInMeters(overlay_handle, 4);
    sim_view::check_overlay_error();
    std::cout << "Set width\n";
    // relative transform
    sim_view::find_hmd();
	vr::HmdMatrix34_t transform = {
		1.0f, 0.0f, 0.0f, 0.2f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, -2.0f
	};    
    overlay_error = vr::VROverlay()->SetOverlayTransformTrackedDeviceRelative(overlay_handle, hmd_id, &transform);
    sim_view::check_overlay_error();
    std::cout << "Set transform\n";
	overlay_error = vr::VROverlay()->ShowOverlay(overlay_handle);
    sim_view::check_overlay_error();
    std::cout << "Showing\n";
}

// destructor
sim_view::~sim_view(){
    if (vr_system != NULL){
        vr::VR_Shutdown();
        vr_system = NULL;
    }
}

// read error code
void sim_view::check_overlay_error(){
    if (overlay_error != vr::VROverlayError_None){
        vr_system = NULL;
        std::cout << "Unable to manipulate overlay: " << vr::VROverlay()->GetOverlayErrorNameFromEnum(overlay_error) << "\n";
        exit(EXIT_FAILURE);        
    }
}

// get index of tracked HMD
void sim_view::find_hmd(){
    for (unsigned int i=0; i< vr::k_unMaxTrackedDeviceCount; i++){
        if (!vr_system->IsTrackedDeviceConnected(i))
            continue;
        if (vr_system->GetTrackedDeviceClass(i) != vr::TrackedDeviceClass_HMD)
            continue;
        hmd_id = i;
        break;
    }
}

// read image data
void sim_view::read_raw(){
    buf = new uint8_t[900*6400];
    // fill buf
    for (unsigned int i=0; i<5760000; i++){
        buf[i]=0;
    }
}

// render current available image
void sim_view::run(){
    overlay_error = vr::VROverlay()->ClearOverlayTexture(overlay_handle);
    sim_view::check_overlay_error();
    sim_view::read_raw();
    overlay_error = vr::VROverlay()->SetOverlayRaw(overlay_handle, buf, 1600, 900, 4);
    sim_view::check_overlay_error();
}
