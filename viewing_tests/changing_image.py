import shutil
import time

if __name__ == "__main__":
    path_image_1 = "/home/user/moraw/isaacgym-vrviewer/viewer.png"
    path_image_2 = "/home/user/moraw/isaacgym-vrviewer/default.png"
    path_change = "/home/user/moraw/isaacgym-vrviewer/overlay_image.png"
    shutil.copyfile(path_image_1, path_change)
    time.sleep(10)
    shutil.copyfile(path_image_2, path_change)
    time.sleep(10)
    shutil.copyfile(path_image_1, path_change)
