import numpy as np
from PIL import Image

"""
IMAGE_COLOR: 4x8bit unsigned int, RGBA color
"""

image_array = np.loadtxt('imagedata_2.txt', dtype=np.uint8)
image_array = image_array.reshape((900, 1600, 4))
i = Image.fromarray(image_array, 'RGBA')
i.show()