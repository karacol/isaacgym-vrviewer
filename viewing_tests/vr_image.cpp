#include <stdio.h>
#include <openvr.h>
#include <chrono>
#include <thread>
#include <iostream>

int main(int argc, char **argv) { (void) argc; (void) argv;
    vr::HmdError error;
    vr::IVRSystem* vr_system = NULL;
	
	vr_system = vr::VR_Init(&error, vr::VRApplication_Overlay);

    if (error != vr::VRInitError_None){
        vr_system = NULL;
        std::cout << "Unable to initialise VR runtime: " << vr::VR_GetVRInitErrorAsEnglishDescription(error) << "\n";
        exit(EXIT_FAILURE);
    }	

	vr::VROverlayHandle_t handle;
	vr::VROverlay()->CreateOverlay ("image", "image", &handle); /* key has to be unique, name doesn't matter */
	vr::VROverlay()->SetOverlayFromFile(handle, "/home/user/moraw/isaacgym-vrviewer/overlay_image.png");
	vr::VROverlay()->SetOverlayWidthInMeters(handle, 3);
	vr::VROverlay()->ShowOverlay(handle);

	vr::HmdMatrix34_t transform = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, -2.0f
	};
	vr::VROverlay()->SetOverlayTransformAbsolute(handle, vr::TrackingUniverseStanding, &transform);

	while (true) {
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
		vr::VROverlay()->SetOverlayFromFile(handle, "/home/user/moraw/isaacgym-vrviewer/overlay_image.png");
	}
	return 0;
}
