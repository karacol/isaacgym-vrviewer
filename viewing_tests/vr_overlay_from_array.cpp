#include <openvr.h>
#include <chrono>
#include <thread>
#include <iostream>
#include <fstream>

uint8_t* read_input_data(){
   std::ifstream inputfile("/home/user/moraw/isaacgym-vrviewer/viewing_tests/imagedata.txt");
    uint8_t* buf = new uint8_t[900*6400];
    int number;
    int i = 0;
    // read in input data
    if(inputfile){
        while (inputfile >> number){
            buf[i] = number;
            i++;
        }
        std::cout << "Success\n";
    }
    else{
        std::cout << "Problem\n";
    }
    inputfile.close();
    return buf;
}

void check_overlay_error(vr::VROverlayError overlay_error){
    if (overlay_error != vr::VROverlayError_None){
        std::cout << "Unable to manipulate overlay: " << vr::VROverlay()->GetOverlayErrorNameFromEnum(overlay_error) << "\n";
        exit(EXIT_FAILURE);        
    }
}

vr::TrackedDeviceIndex_t find_hmd(vr::IVRSystem* vr_system){
    vr::TrackedDeviceIndex_t hmd_id;
    for (unsigned int i=0; i< vr::k_unMaxTrackedDeviceCount; i++){
        if (!vr_system->IsTrackedDeviceConnected(i))
            continue;
        if (vr_system->GetTrackedDeviceClass(i) != vr::TrackedDeviceClass_HMD)
            continue;
        hmd_id = i;
        break;
    }
    return hmd_id;
}

int main(){
    uint8_t* buf = read_input_data();
    vr::HmdError error;
    vr::VROverlayError overlay_error;
    vr::IVRSystem* vr_system = NULL;
	
	vr_system = vr::VR_Init(&error, vr::VRApplication_Overlay);

    if (error != vr::VRInitError_None){
        vr_system = NULL;
        std::cout << "Unable to initialise VR runtime: " << vr::VR_GetVRInitErrorAsEnglishDescription(error) << "\n";
        exit(EXIT_FAILURE);
    }	

	vr::VROverlayHandle_t handle;
	overlay_error = vr::VROverlay()->CreateOverlay ("raw", "raw", &handle); /* key has to be unique, name doesn't matter */
	check_overlay_error(overlay_error);
    std::cout << "Created overlay\n";
    overlay_error = vr::VROverlay()->SetOverlayWidthInMeters(handle, 3.9);
    check_overlay_error(overlay_error);
    std::cout << "Set Width\n";


    overlay_error = vr::VROverlay()->SetOverlayRaw(handle, buf, 1600, 900, 4);
    check_overlay_error(overlay_error);
    std::cout << "Set Raw\n";

    // relative transform
    vr::TrackedDeviceIndex_t hmd_id;
    hmd_id = find_hmd(vr_system);
	vr::HmdMatrix34_t transform = {
		1.0f, 0.0f, 0.0f, 0.2f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, -2.0f
	};    
    overlay_error = vr::VROverlay()->SetOverlayTransformTrackedDeviceRelative(handle, hmd_id, &transform);
    check_overlay_error(overlay_error);
    std::cout << "Set transform\n";
    
    overlay_error = vr::VROverlay()->ShowOverlay(handle);
    check_overlay_error(overlay_error);
    std::cout << "Showing\n";
    
	int counter = 0;
	while (counter < 1000) {
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
		counter ++;
	}
    return 0;
}
